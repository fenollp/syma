#syma • [Bitbucket](https://bitbucket.org/fenollp/syma)

## TP1 ~ a game of life

### How To Use:
* Compile and run in the Erlang shell: `make debug`
* Step-by-step game:

```erlang
tp1:start(10). %% Spawns a game table and its 10*10 cells
tp1:print().   %% Display initial table filing
tp1:next().
tp1:print().   %% Play Life once and display table.
tp1:stop().    %% Un-register global pid 'table'.
```

* Auto game:

```erl
tp1:start(50). %% Table + 2500 cells. (each an agent)
tp1:play(200). %% step+print every 200 milliseconds.
```

* End|Close shell with `^G q` or `^C^C^C`
