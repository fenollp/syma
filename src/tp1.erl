%% Copyright © 2014 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(tp1).

%% tp1: a game of life

-export([ start/1
        , next/0
        , print/0
        , stop/0
        , play/1 ]).


-define(W, 35). %% '#'
-define(_, 32). %% ' '

%% Comment|Uncomment for verbose debug output
%-define(f(L), io:format("== ~p\n", [L]),).
-define(f(_), ).
%%

-record(s, { x, y
           , type          :: ?_ | ?W
           , '#neighbours' :: pos_integer()
           , '#alive'      :: pos_integer()
           }).

%% API

start (M) ->
    spawn(fun () -> table(M) end).

play (Interval) ->
    print(),
    next(),
    timer:sleep(Interval),
    play(Interval).

next () ->
    table ! step,
    ok.

print () ->
    table ! {size, self()},
    M = receive
            {size, M_} -> M_
        end,
    MM = M * M,
    [table ! {lookup, x(I,M), y(I,M), self()} || I <- lists:seq(1, MM)],
    gather(MM, M, erlang:make_tuple(MM, $A)).

stop () ->
    table ! stop,
    true = unregister(table),
    ok.

%% Internals

table (M) ->
    true = register(table, self()),
    Pids = [begin
                case random:uniform() of
                    Big when Big < 0.5 -> Type = ?W;
                    __________________ -> Type = ?_
                end,
                spawn(fun () ->
                              cell(M, #s{ type          = Type
                                        , x             = x(I,M)
                                        , y             = y(I,M)
                                        , '#neighbours' = 0
                                        , '#alive'      = 0
                                        })
                      end)
            end || I <- lists:seq(1, M * M)],
    loop(M, Pids).

loop (M, Pids) ->
    receive
        step ->
            [Pid ! step || Pid <- Pids],
            loop(M, Pids);
        {lookup, X, Y, _From}=Msg ->
            lists:nth(i(X,Y,M), Pids) ! Msg,
            loop(M, Pids);
        {size, From} ->
            From ! {size, M},
            loop(M, Pids);
        stop ->
            [Pid ! stop || Pid <- Pids],
            ok;
        _M ->
            io:format("E ~p uncaught: ~p\n", [self(), _M]),
            loop(M, Pids)
    end.

cell (M, S) ->
    receive
        step -> %% Start looking for neighbours
            [table ! {lookup, XX, YY, self()}
             || {XX,YY} <- neighbours(S#s.x, S#s.y, M)],
            cell(M, S);
        {lookup, X, Y, From} -> %% Answer to neighbours
            From ! {type, X, Y, S#s.type},
            cell(M, S);
        {type, _, _, Type} -> %% Neighbours answer
            AmountAlive = S#s.'#alive',
            case S#s.'#neighbours' + 1 of
                8 ->
                    Left = AmountAlive + alive(Type),
                    NewType = live(S#s.type, Left),
                    cell(M, S#s{'#neighbours'=0,             '#alive'=0, type=NewType});
                New_NbrsCount ->
                    cell(M, S#s{'#neighbours'=New_NbrsCount, '#alive'=AmountAlive})
            end;
        stop ->
            ok;
        _M ->
            io:format("E ~p uncaught: ~p\n", [self(), _M]),
            cell(M, S)
    end.

neighbours (X, Y, M) ->
    L = case X of 1 -> M; _ -> X -1 end, %% Left
    R = case X of M -> 1; _ -> X +1 end, %% Right
    U = case Y of 1 -> M; _ -> Y -1 end, %% Up
    D = case Y of M -> 1; _ -> Y +1 end, %% Down
    [ {L,U}, {X,U}, {R,U}
    , {L,Y},        {R,Y}
    , {L,D}, {X,D}, {R,D} ].

alive (?W) -> 1;
alive (?_) -> 0.

live (?W, Neighbours) when Neighbours <  2 -> ?_;
live (?W, Neighbours) when Neighbours >  3 -> ?_;
live (?_, Neighbours) when Neighbours == 3 -> ?W;
live (Ty, _) -> Ty.


gather (0, M, T) ->
    finish_print(M, tuple_to_list(T));
gather (Acc, M, T) ->
    receive
        {type, X, Y, Type} ->
            gather(Acc -1, M, setelement(i(X,Y,M), T, Type));
        _M ->
            gather(Acc -1, M, T)
    end.

finish_print (_, []) -> ok;
finish_print (M, L) ->
    {MSized, Rest} = lists:split(M, L),
    io:fwrite("|~p|\n", [MSized]),
    finish_print(M, Rest).


x (I, M) ->
    case I rem M of
        0 -> M;
        R -> R
    end.

y (I, N) ->
    case {I div N, I rem N} of
        {D,0} -> D;
        {R,_} -> R +1
    end.

i (X, Y, M) ->
    M * (Y -1) + X.

%% X,Y --> N
%% |
%% ~    M,N
%% M

%% End of Module.
